﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PangyaAPI;
using PangyaAPI.BinaryModels;
using PangyaAPI.Repository.Models;

namespace Pangya_Season7_GS.Handles_Packet
{
    public class Packet_PLAYER_CHANGE_EQUIPMENTS : PacketResult
    {
        public enum ChangeEquipmentEnum : byte
        {
            /// <summary>
            /// Salva a lista de roupas selecionadas 
            /// </summary>
            SetEquip_Char = 0,
            /// <summary>
            /// Seta Index do Caddie
            /// </summary>
            SetIndexCaddie = 1,
            /// <summary>
            /// Salva uma lista de itens do tipo active no inventario
            /// </summary>
            SetItensPlay = 2,
            /// <summary>
            /// Salva index do taco selecionado, e typeID da Bolinha
            /// </summary>
            SetGolfEQP = 3,
            /// <summary>
            /// Salva a decoração do armario, title, painel de fundo etc...
            /// </summary>
            SetDecoration = 4,
            /// <summary>
            ///  Seta Index do Character
            /// </summary>
            SetIndexChar = 5,
            /// <summary>
            ///  Seta Index do Mascot
            /// </summary>
            SetIndexMascot = 8,
            /// <summary>
            ///  Seta Index do Cutin no Character
            /// </summary>
            SetCharCutin = 9
        }

        public ChangeEquipmentEnum Action { get; set; }

        public Character Character { get; set; }

        public int CaddieId { get; set; }

        public int MascoteId { get; set; }


        public override void Load(PangyaBinaryReader reader)
        {
            Action = (ChangeEquipmentEnum)reader.ReadByte();

            switch (Action)
            {
                case ChangeEquipmentEnum.SetEquip_Char: 
                    {
                        this.Character = new Character();

                        Character.TYPEID = reader.ReadInt32();
                        Character.CID = reader.ReadInt32();

                        Character.HAIR_COLOR = reader.ReadUInt16();

                        var default_shirts = reader.ReadUInt16();

                        Character.GIFT_FLAG = reader.ReadUInt16();

                        var purchase = reader.ReadUInt16();

                        //Parts typeid, do 1 ao 24
                        for (int i = 0; i < 24; i++)
                        {
                            Character.CharacterEquip = new CharacterEquip();
                            Character.CharacterEquip.SetPartTYPEID(i, reader.ReadUInt32());
                        }

                        //Parts ID, do 1 ao 24
                        for (int i = 0; i < 24; i++)
                        {
                            Character.CharacterEquip.SetPartIDX(i, reader.ReadUInt32());
                        }

                        var cblank1 = reader.ReadBytes(216); //Não sei bem direito o que é aqui

                        // Auxiliar Parts 5, aqui fica anel
                        var auxparts = new int[5];

                        for (int i = 0; i < auxparts.Length; i++)
                        {
                            auxparts[i] = reader.ReadInt32();
                        }

                        // Cut-in, no primeiro mas acho que pode ser cut-in no resto
                        var cut_in = new int[4];

                        for (int i = 0; i < cut_in.Length; i++)
                        {
                            cut_in[i] = reader.ReadInt32();
                        }

                        // Aqui é o character stats, como controle, força, spin e etc
                        var pcl = new byte[5];

                        for (int i = 0; i < pcl.Length; i++)
                        {
                            pcl[i] = reader.ReadByte();
                        }

                        // Mastery, que aumenta os slot do stats do character
                        int mastery = reader.ReadInt32();

                        // 4 Slot de card Character
                        var card_character = new int[4];

                        for (int i = 0; i < card_character.Length; i++)
                        {
                            card_character[i] = reader.ReadInt32();
                        }


                        // 4 Slot de card Caddie
                        var card_caddie = new int[4];

                        for (int i = 0; i < card_caddie.Length; i++)
                        {
                            card_caddie[i] = reader.ReadInt32();
                        }

                        // 4 Slot de card NPC
                        var card_NPC = new int[4];

                        for (int i = 0; i < card_NPC.Length; i++)
                        {
                            card_NPC[i] = reader.ReadInt32();
                        }

                    }
                    break;
                case ChangeEquipmentEnum.SetIndexCaddie:
                    {
                        CaddieId = reader.ReadInt32();
                    }
                    break;
                case ChangeEquipmentEnum.SetItensPlay:
                    break;
                case ChangeEquipmentEnum.SetGolfEQP:
                    break;
                case ChangeEquipmentEnum.SetDecoration:
                    break;
                case ChangeEquipmentEnum.SetIndexChar:
                    break;
                case ChangeEquipmentEnum.SetIndexMascot:
                    {
                        MascoteId = reader.ReadInt32();
                    }
                    break;
                case ChangeEquipmentEnum.SetCharCutin:
                    break;
                default:
                    break;
            }
        }
    }
}
