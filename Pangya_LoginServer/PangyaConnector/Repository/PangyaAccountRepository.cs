﻿using Dapper;
using PangyaConnector.Properties;
using PangyaConnector.Repository.Model;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PangyaConnector.Repository
{
    public class PangyaAccountRepository
    {
        private readonly string _connectionString;

        public PangyaAccountRepository()
        {
            _connectionString = Settings1.Default.ConnectionString;
        }


        public PangyaAccountModel GetByUserName(string userName)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                connection.Open();

                var query = @" SELECT * FROM pangya.account WHERE Id = @userName";

                return connection.Query<PangyaAccountModel>(query, new { userName }).FirstOrDefault();
            }
        }

        public PangyaAccountModel GetByUID(int UID)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                connection.Open();

                var query = @" SELECT * FROM pangya.account WHERE UID = @UID";

                return connection.Query<PangyaAccountModel>(query, new { UID }).FirstOrDefault();
            }
        }
    }
}
