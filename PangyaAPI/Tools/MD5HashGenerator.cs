﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace PangyaAPI.Tools
{
    public static class MD5HashGenerator
    {
        /// <summary>
        /// Criptografa string em MD5 Hash
        /// </summary>
        public static string ToMd5(this string input, bool isLowercase = true)
        {
            using (var md5 = MD5.Create())
            {
                var byteHash = md5.ComputeHash(Encoding.UTF8.GetBytes(input));
                var hash = BitConverter.ToString(byteHash).Replace("-", "");
                return (isLowercase) ? hash.ToLower() : hash;
            }
        }
    }
}
